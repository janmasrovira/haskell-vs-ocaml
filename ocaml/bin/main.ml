open Core

let () =
  (* Read JSON file into an OCaml string *)
  let buf = In_channel.(input_line_exn stdin) in
  (* Use the string JSON constructor *)
  let json1 = Yojson.Basic.from_string buf in
  let open Yojson.Basic.Util in
  let a_num = member "a" json1 |> to_int in
  let b_num = member "b" json1 |> to_int in
  let output = `Assoc [("result", `Int (a_num + b_num))] in
  (* print_endline (string_of_int (a_num + b_num)); *)
  print_endline (Yojson.Basic.to_string output);
  (* Yojson.Basic.to_file "result.json" output *)
