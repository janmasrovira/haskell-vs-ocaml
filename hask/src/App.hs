{-# LANGUAGE DeriveGeneric #-}
module App
    ( main
    , readInt
    ) where

import Data.Aeson
import GHC.Generics
import System.Directory
import System.Process.Typed

data Obj = Con {
  a :: Int
  , b :: Int
  }
  deriving (Show, Generic, Eq)

instance ToJSON Obj where
    toEncoding = genericToEncoding defaultOptions
instance FromJSON Obj

newtype Res = Res {
  result :: Int
  }
  deriving (Show, Generic, Eq)

instance ToJSON Res where
    toEncoding = genericToEncoding defaultOptions
instance FromJSON Res

-- | Parses an 'Int' from a 'String'.
readInt :: String -> Int
readInt = read

class Monad m => MonadQuery m where
  queryA :: Int -> Int -> m String
  queryB :: Int -> Int -> m Int

instance MonadQuery IO where
  queryA a b =
    withCurrentDirectory "../ocaml" $ do
    (_exitCode, out, _err) <- readProcess
      $ setStdin (byteStringInput e)
      (proc "dune" ["exec", "bin/main.exe"])
    case decode out of
      Just res -> return (show (result res))
      Nothing -> return "parse error"
    where e = encode (Con a b)

  queryB a b = return b

process :: MonadQuery m => Int -> Int -> m String
process a b = do
  x <- queryA a b
  y <- queryB a b
  return x

main :: IO ()
main = do
  putStrLn "Input two numbers:"
  (x : y : _) <- map readInt . words <$> getLine
  res <- process x y
  putStrLn res
